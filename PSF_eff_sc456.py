
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

@author: sclery
"""

import os
import FITS
import astropy.io.fits as fits
import numpy as np
import numpy.fft as npf
import scipy.interpolate as inter
import glob
import sys





def dlambda_value(nbr_pts, psf_file):
    """
    Give the value of the step for the integral

    Parameters:
    ==========
    
    nbr_pts : int
              The number of the square that we want for the integral computing
    psf_file : file .fits
               The PSF file
    Return :
    ======
    dlambda : int
              The step

    """
    
    PSFwavelengh = np.array([ 550.0, 585.2, 625.21367521, 671.10091743, 724.25742574,786.55913978, 860.58823529, 950.0])
    size_tab = len(PSFwavelengh)
    lengh = PSFwavelengh[size_tab - 1] - PSFwavelengh[0]

    dlambda = lengh / nbr_pts

    return dlambda


def stock_relevant_data(data_tab, col):
    """
    Stock the data that we want if we know which column is relevant

    Parameters:
    ==========
    data_tab : nd.array
               The table of all the data that we have.
    col : int
          The number of the column of the relevant data

    Returns:
    =======
    relavant_tab : nd.array
                   The table of the relevant data

    """
    taille = len(data_tab)
    relevant_tab = np.empty(taille)
    for i in range(taille):
        relevant_tab[i] = data_tab[i][col]
    return relevant_tab




def used_through_psf(all_wlgh_tab, inter_wlgh_tab, all_data_tab):
    """
    Compute and return the relevant through data

    Parameters:
    ==========
    all_wlgh_tab : nd.array
                   Table of all wavelengh that we have in the file (through or SED flux)
    inter_wlgh_tab : nd.array
                     Table of the Wavelengh used for the interpolation
    all_data_tab : nd.array
                      Table of all the data that we have in the file (through or SED flux)

    Return:
    ======
    used_data : nd.array
                   The table of the data (through or SED flux) used for the interpolation
    """
    
    all_size = len(all_wlgh_tab)
    inter_size = len(inter_wlgh_tab)
    
    used_through = np.empty(inter_size)
    
    for i, wlgh in enumerate(all_wlgh_tab):
        for j, inter_wlgh in enumerate(inter_wlgh_tab):
            if int(wlgh) == int(inter_wlgh):
                used_through[np.min((i,j))] = all_data_tab[i]

    return used_through





def interp_psf(psf_file,num_case,data_file,nbr_pts,sed_file):
    """
    The interpolation computing

    Parameters :
    =========
    psf_file : file .fits
               The PSF file
    data_file : 
                The Through data dossier VIS ( contient les rdt optiques et les lambda)
    nbr_pts : int 
              The number of point

    Return :
    ======
    
    A tuple
    """
    
    #Parameters from PSF file
    PSF = psf_file
    size_cube = fits.getdata(PSF).shape[0] # Number of pixels for each image (a square)
    psf_wlgh =np.array([ 550.0, 585.2, 625.21367521, 671.10091743, 724.25742574,786.55913978, 860.58823529, 950.0])
    size_psf_wlgh = len(psf_wlgh)
    dlambda = dlambda_value(nbr_pts, PSF)

    #Data from Instrument Data
    through_data = np.loadtxt(data_file)
    wave_lengh = stock_relevant_data(through_data, 0)
    through = stock_relevant_data(through_data, 4)
    
    # Data from SED Data 
             
    sed_data=np.loadtxt(sed_file)
    sed_wlgh=stock_relevant_data(sed_data,0)
    sed_flux=stock_relevant_data(sed_data,1)

  

    #Initial and original data
    interpolation_wlgh = np.arange(psf_wlgh[0], psf_wlgh[size_psf_wlgh-1]+dlambda, dlambda)
    
    size_interp_wlgh = len(interpolation_wlgh)
    used_through = used_through_psf(wave_lengh, interpolation_wlgh, through)
    used_sed=used_through_psf(sed_wlgh,interpolation_wlgh,sed_flux)

    #Initialization of the data_tab
    data_cube_real = np.empty((size_cube, size_cube, size_psf_wlgh)) #taille: 300x300x8
    data_cube_imag = np.empty((size_cube, size_cube, size_psf_wlgh))# taille:300x300x8
    interp_cube_real = np.empty((size_cube, size_cube, size_interp_wlgh))#taille:300x300xnb_de_pts_interpolation
    interp_cube_imag = np.empty((size_cube, size_cube, size_interp_wlgh))#idem    

    
    #Give value to the data tab
    for i, wlgh in enumerate(psf_wlgh):
        data = fits.getdata(PSF)[:,:,i,num_case]# Monochrom PSF
        TF = npf.fftshift(npf.fft2(data)) #Compute the FFTshift of 1 PSF for the case num_case out of 1482
        #TF = npf.fft2(data)
        data_cube_real[:, :, i] = np.real(TF)#taille:300x300
        data_cube_imag[:, :, i] = np.imag(TF)#idem
        
    #Computing the interpolation    
    for i in range(size_cube):
        for j in range(size_cube):
                
            fct_interp_real = inter.InterpolatedUnivariateSpline(psf_wlgh, data_cube_real[i, j, :])
            fct_interp_imag = inter.InterpolatedUnivariateSpline(psf_wlgh, data_cube_imag[i, j, :])

            interp_cube_real[i, j, :] = fct_interp_real(interpolation_wlgh)
            interp_cube_imag[i, j, :] = fct_interp_imag(interpolation_wlgh)

    #Interpolated data tab
    interp_cube = np.vectorize(complex)(interp_cube_real, interp_cube_imag)

    
    return used_through, interpolation_wlgh, interp_cube, used_sed






def main(PSF_EFF_PATH,num_CCD,SED_type):
    """

   - PSF_EFF_PATH is the path to save PSF_eff files for different SED type
    Examples:
    'PSF_eff_sc456_SED_M/PSF_eff_sc456_SED_M_CCD00'

    - num_CCD is the numero of the CCD that we choose
    Example :
    '01' to '05' then '10' to '15' etc... to '55'
    
    -SED_type is the spectral type of the sources
    Example:
    'SED-M_ind-2583.txt' or 'SED-O_ind-3928.txt'
    """

    
    PSF_PATH = '/data/glx-herschel/data2/data_lodeen/data_sc456/psf_simulated_sc456/'

    os.mkdir(str(PSF_EFF_PATH))

 
    
    list_PSF_eff = glob.glob(PSF_EFF_PATH+'/*')

    
    PSF= PSF_PATH+'PSF_CCD_ccd'+str(num_CCD)+'.fits'
    PSF_name=PSF.replace('.fits','')
    PSF_name=PSF_name.replace('/data/glx-herschel/data2/data_lodeen/data_sc456/psf_simulated_sc456','')
    header=fits.open(PSF)[1].header
    positions=fits.open(PSF)[2].data
    header['PIXSCALE'] =' 4.0 /pixel size in micrometers'
    header['SEDTYPE']=SED_type
        
    del header['PCOUNT']
    del header['GCOUNT']
    del header['AXIS1']
    del header['AXIS2']
        
    for num_case in range (len(positions)):
            
        x=positions[num_case][1]
        y=positions[num_case][2]
        header['XFIELD']=x
        header['YFIELD']=y
        name = PSF_EFF_PATH+PSF_name +'_X%f_Y%f.fits'%(x, y)
        
          
        if name not in list_PSF_eff:

            sed_data='SED/'+str(SED_type)
            through_data = 'Python/6_PSF_effectives/VISThroughBE.dat'        
            interp_data = interp_psf(PSF,num_case,through_data,30,sed_data)
    
            dlambda = dlambda_value(30, PSF)
            sum_interp_cube = dlambda * (interp_data[0] / interp_data[1]) * interp_data[2] *interp_data[3]
            sum_through = dlambda * (interp_data[0] / interp_data[1])*interp_data[3]
                
            #Computing the norm
            norm = sum_through.sum()
            
            #Computing the PSF sum
            sum_psf = sum_interp_cube.sum(axis=2)
        
                
            #Computing the effective PSF
            PSF_effective = np.fabs(np.real(npf.ifft2(sum_psf / norm))) #### ajout fabs()
                
            print(num_case)
        
                
            fits.writeto(name, PSF_effective, header)
            list_PSF_eff.append(name)

              


if __name__ == '__main__':

    
    SED_type=sys.argv[3]
    PSF_EFF_PATH=sys.argv[1]
    num_CCD=sys.argv[2] 
    print(PSF_EFF_PATH)
    print(num_CCD)
    print(SED_type)
    main(PSF_EFF_PATH,num_CCD,SED_type)
