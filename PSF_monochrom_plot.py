
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 13:57:49 2019

@author: sclery
"""

import astropy.io.fits as fits
import numpy as np
import FITS as FITS
import matplotlib.pyplot as pl



def PSF_monochrom_1_case_image(fits_name,num_wavelength,num_case):

    """
    return image of one PSF monochromatic (1/8) for 1 case of 1 CCD (1/1482)

    """
    fits_cube=FITS.data_record(fits_name)[1]
    image=fits_cube[:,:,num_wavelength,num_case]
    return image



