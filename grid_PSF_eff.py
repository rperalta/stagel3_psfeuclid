import os
import astropy.io.fits as fits
import numpy as np
import parameters as p
import matplotlib.pyplot as pl
import math as m

# PATH


PATH="PSF_eff_sc456_SED_O/"


'''
# Grid 1x1
def compute_mean_PSF_grid(PATH,num_CCD):

    
    PSF_PATH=PATH+"PSF_eff_sc456_SED_M_CCD"+num_CCD
    
    allfiles = os.listdir(PSF_PATH)
    PSFfiles = [os.path.join(PSF_PATH,fil)
                for fil in allfiles
                if fil.endswith('.fits')]
    
    
    image=fits.open(PSFfiles[0])[0].data
    nx,ny=image.shape
    sum_image=np.zeros((nx,ny))
    
    for psf in enumerate(PSFfiles):
        image=fits.open(psf)[0].data
        sum_image=sum_image+image
        
        
    return sum_image/(len(PSFfiles))

'''


# Grid grid_size x grid_size

def compute_grid(PATH,num_CCD,grid_size):

    """
    This function cut a CCD in a grid 

    Arguments:
    PATH is the path to access all the files
    numm_CCD is the number of the CCD
    ex: '00' or '01'
    grid_size is the size of the grid we want to cut the CCD
    ex: 3 for a 3x3 grid or 9 for a 9x9 grid

    return:
    Image_moy is a list of grid_size x grid_size elements. Each elements is an averaged PSF
    Xmoy is a list of grid_size x grid_size elements. Each elements is the average position of the PSF on axis x
    Ymoy is a list of grid_size x grid_size elements. Each elements is the average position of the PSF on axis y
    """
  
    PSF_PATH=PATH+"PSF_eff_sc456_SED_O_CCD"+num_CCD
    
    allfiles = os.listdir(PSF_PATH)
    PSFfiles = [os.path.join(PSF_PATH,fil)
                for fil in allfiles
                if fil.endswith('.fits')]
    
    taille_ccd=len(PSFfiles)
    taille_cote=int(m.sqrt(taille_ccd))
    maille=int(taille_cote*1./grid_size)
    X=[]
    Y=[]
    image=fits.open(PSFfiles[0])[0].data
    nx,ny=image.shape
    sum_image=np.zeros((nx,ny))
    moy=np.zeros((nx,ny))
    xmoy,ymoy=0,0
    Xmoy=[]
    Ymoy=[]
    Image_moy=[]
    for k in range(taille_ccd):
         header=fits.open(PSFfiles[k])[0].header
         X.append(header['XFIELD'])
         Y.append(header['YFIELD'])

         
    xmin,ymin=np.min(X),np.min(Y)
    xmax,ymax=np.max(X),np.max(Y)
    ax,ay=(xmax-xmin)/grid_size,(ymax-ymin)/grid_size
   
    for k in range(grid_size):
        for j in range(grid_size):
            for i in range(taille_ccd):
                header=fits.open(PSFfiles[i])[0].header
                if xmin+k*ax<header['XFIELD']<xmin+(k+1)*ax and  ymin+j*ay<header['YFIELD']<ymin+(j+1)*ay:
                    image=fits.open(PSFfiles[i])[0].data
                    sum_image=sum_image+image
                    xmoy=xmoy+header['XFIELD']
                    ymoy=ymoy+header['YFIELD']
                    
            moy=sum_image/(maille**2)
            xmoy=xmoy/(maille**2)
            ymoy=ymoy/(maille**2)
            Xmoy.append(xmoy)
            Ymoy.append(ymoy)
            Image_moy.append(moy)

            
    return (Image_moy,Xmoy,Ymoy)



def ordonner(PATH,num_CCD,grid_size):

    '''
    This function reorganize a grid

    Arguments:
    PATH is the path to access all the files
    numm_CCD is the number of the CCD
    ex: '00' or '01'
    grid_size is the size of the grid we want to cut the CCD
    ex: 3 for a 3x3 grid or 9 for a 9x9 grid

    donnes is the result of the function compute_grid
    image correspond to Image_moy is comput_grid
    grille is an empty matrix whiche will contain image elements but reorganised

    return:
    grille
    '''
    donnes=compute_grid(PATH,num_CCD,grid_size)
    image=donnes[0]
    image=image/np.max(image)
    grille=np.empty((grid_size,grid_size),np.ndarray)
    
    for j in range(grid_size):
        for i in range(grid_size-1,-1,-1):
            grille[i,j]=image[j*grid_size+abs(i-grid_size+1)]
        print(j)
                       
    return grille


             

def compute_FPA(PATH,grid_size):

    """
    This function cut the FPA in a grid 

    Arguments:
    PATH is the path to access all the files
 
    grid_size is the size of the grid we want to cut each CCD
    ex: 3 for a 3x3 grid or 9 for a 9x9 grid

    """

    FPA=np.empty((6,6),np.ndarray)
    for i in range(6):
        for j in range(6):
            FPA[i][j]=ordonner(PATH,str(i)+str(j),grid_size)

    return FPA
            





    
    
    
    
    
    
    
    
