
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 13:57:49 2019

@author: sclery
"""

import astropy.io.fits as fits
import numpy as np


                       # FITS commands on fits files



                       

def FITS_read(fits_name):

    """
    *** Read and list the info of a FITS file entitled 'fits_name'

    """
    hdulist = fits.open(fits_name)
    hdulist.info()

    

def data_record(fits_name):

    """
    *** record data from FITS file as a tuple (header,image)

    """
    image,header=fits.getdata(fits_name,header=True)
    return (header,image)


def list_fits():

    """
    *** Return list of PSF fits file for data sc456 in psf_simulated_sc456

    """
    path="/data/glx-herschel/data2/data_lodeen/data_sc456/psf_simulated_sc456"
    fits_names=[]
    for i in range(0,6):
        for j in range(0,6):
            fits_names.append(path+"/PSF_CCD_ccd"+str(i)+str(j)+".fits")
    return fits_names

def cursor(image,x_fixed,y_fixed):

    """
    ***  Cursor to  locate pixel : tuple (x_cursor,y_cursor) on the image
    
    """
    x_cursor,y_cursor=[],[]
    for i in range(len(image[0])):
        x_cursor.append(x_fixed)
    for j in range (len(image[1])):
        y_cursor.append(y_fixed)
    return (x_cursor,y_cursor)



def position(fits_name):

    """
    *** record SC3 data positions from the header as a tuple (x,y)

    """
    
    header=data_record(fits_name)[0]
    x_position=header['XFIELD']
    y_position=header['YFIELD']
    return (x_position,y_position)



