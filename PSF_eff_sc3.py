
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 13:57:49 2019

@author: sclery
"""

import os
import astropy.io.fits as fits
import numpy as np
import numpy.fft as npf
import scipy.interpolate as inter
import glob

def wave_length_psf(psf_file):
    """
    Save for one PSF the Wavelengh information from the header key [WLGH0]

    Parameters:
    ==========
    psf_file: file .fits
              The PSF file that we want to study
    
    Returns:
    =======
    psf_wlgh: nd.array
              The table of the wavelengh
    """
    hdulist = fits.open(psf_file)

    psf_wlgh = np.empty(len(hdulist)-1)

    for i in range(len(hdulist)-1):
        header = fits.getheader(psf_file, i+1)
        psf_wlgh[i] = header['WLGTH0'] * 1000
    
    hdulist.close()
    
    return psf_wlgh



def dlambda_value(nbr_pts, psf_file):
    """
    Give the value of the step for the integral

    Parameters:
    ==========
    
    nbr_pts : int
              The number of the square that we want for the integral computing
    psf_file : file .fits
               The PSF file
    Return :
    ======
    dlambda : int
              The step

    """
    
    PSFwavelengh = wave_length_psf(psf_file)
    size_tab = len(PSFwavelengh)
    lengh = PSFwavelengh[size_tab - 1] - PSFwavelengh[0]

    dlambda = lengh / nbr_pts

    return dlambda


def stock_relevant_data(data_tab, col):
    """
    Stock the data that we want if we know which column is relevant

    Parameters:
    ==========
    data_tab : nd.array
               The table of all the data that we have.
    col : int
          The number of the column of the relevant data

    Returns:
    =======
    relavant_tab : nd.array
                   The table of the relevant data

    """
    taille = len(data_tab)
    relevant_tab = np.empty(taille)
    for i in range(taille):
        relevant_tab[i] = data_tab[i][col]
    return relevant_tab




def used_through_psf(all_wlgh_tab, inter_wlgh_tab, all_through_tab):
    """
    Compute and return the relevant through data

    Parameters:
    ==========
    all_wlgh_tab : nd.array
                   Table of all wavelengh that we have in the Through file
    inter_wlgh_tab : nd.array
                     Table of the Wavelengh used for the interpolation
    all_through_tab : nd.array
                      Table of all the through that we have in the Through file

    Return:
    ======
    used_through : nd.array
                   The table of the through used for the interplation
    """
    
    all_size = len(all_wlgh_tab)
    inter_size = len(inter_wlgh_tab)
    
    used_through = np.empty(inter_size)
    
    for i, wlgh in enumerate(all_wlgh_tab):
        for j, inter_wlgh in enumerate(inter_wlgh_tab):
            if wlgh == inter_wlgh:
                used_through[np.min((i,j))] = all_through_tab[i]

    return used_through





def interp_psf(psf_file, data_file,  nbr_pts):
    """
    The interpolation computing

    Parameters :
    =========
    psf_file : file .fits
               The PSF file
    data_file : 
                The Through data 
    nbr_pts : int 
              The number of point

    Return :
    ======
    
    A tuple
    """
    
    #Parameters from PSF file
    PSF = psf_file
    hdulist = fits.open(PSF)
    nbr_data = (len(hdulist) - 1)
    hdulist.close()
    size_cube = len(fits.getdata(PSF))

    #Data from Instrument Data
    through_data = np.loadtxt(data_file)
    wave_lengh = stock_relevant_data(through_data, 0)
    through = stock_relevant_data(through_data, 4)

    #Data from PSF file
    psf_wlgh = wave_length_psf(PSF)
    size_psf_wlgh = len(psf_wlgh)
    dlambda = dlambda_value(nbr_pts, PSF)

    #Initial and original data
    interpolation_wlgh = np.arange(psf_wlgh[0], psf_wlgh[size_psf_wlgh-1]+dlambda, dlambda)
    size_interp_wlgh = len(interpolation_wlgh)
    used_through = used_through_psf(wave_lengh, interpolation_wlgh, through)

    #Initialization of the data_tab
    data_cube_real = np.empty((size_cube, size_cube, size_psf_wlgh))
    data_cube_imag = np.empty((size_cube, size_cube, size_psf_wlgh))
    interp_cube_real = np.empty((size_cube, size_cube, size_interp_wlgh))
    interp_cube_imag = np.empty((size_cube, size_cube, size_interp_wlgh))    

    
    #Give value to the data tab
    for i, wlgh in enumerate(psf_wlgh):
        data = fits.getdata(PSF, i+1)
        TF = npf.fftshift(npf.fft2(data))
        #TF = npf.fft2(data)
        data_cube_real[:, :, i] = np.real(TF)
        data_cube_imag[:, :, i] = np.imag(TF)
        
    #Computing the interpolation    
    for i in range(size_cube):
        for j in range(size_cube):
                
            fct_interp_real = inter.InterpolatedUnivariateSpline(psf_wlgh, data_cube_real[i, j, :])
            fct_interp_imag = inter.InterpolatedUnivariateSpline(psf_wlgh, data_cube_imag[i, j, :])

            interp_cube_real[i, j, :] = fct_interp_real(interpolation_wlgh)
            interp_cube_imag[i, j, :] = fct_interp_imag(interpolation_wlgh)

    #Interpolated data tab
    interp_cube = np.vectorize(complex)(interp_cube_real, interp_cube_imag)

    return used_through, interpolation_wlgh, interp_cube





def main():

    PSF_PATH = '/data/glx-herschel/data2/data_lodeen/data_sc3/psfs_simulated_sc3/grille40x40/'
    PSF_EFF_PATH = 'PSF_eff_sc3'
    
    # Listing des fichiers
    allfiles = os.listdir(PSF_PATH)
    PSFfiles = [os.path.join(PSF_PATH,fil)
                for fil in allfiles
                if fil.endswith('.fits')]
    
    list_PSF_eff = glob.glob(PSF_EFF_PATH+'/*')
    
    for i, PSF in enumerate(PSFfiles):
        
        
        print(i)
        
        
        header = fits.getheader(PSF,1)
        x = header['XFIELD']
        y = header['YFIELD']
        
        header['PIXSCALE'] = 0.0083*2 #taille du pixel en arcsec
        
        del header['WLGTH0']
        del header['CATALOG']
        del header['CATSEQ']
        
        name = PSF_EFF_PATH + '/psf_effective_X%f_Y%f.fits'%(x, y)
        
        if name not in list_PSF_eff:
            
            through_data = 'Python/6_PSF_effectives/VISThroughBE.dat'        
            interp_data = interp_psf(PSF, through_data, 30)
            
            dlambda = dlambda_value(30, PSF)
            sum_interp_cube = dlambda * (interp_data[0] / interp_data[1]) * interp_data[2]
            sum_through = dlambda * (interp_data[0] / interp_data[1])
            
            #Computing the norm
            norm = sum_through.sum(axis=0)
            
            #Computing the PSF sum
            sum_psf = sum_interp_cube.sum(axis=2)
            
            #Computing the effective PSF
            PSF_effective = np.fabs(np.real(npf.ifft2(sum_psf / norm))) #### ajout fabs()
    
            fits.writeto(name, PSF_effective, header)


if __name__ == '__main__':
    main()
