import parameters as p
import FITS as FITS
import astropy.io.fits as fits
import PSF_monochrom_plot as PSF
import numpy as np
import matplotlib.pyplot as pl
import os


path='/data/glx-herschel/data2/data_lodeen/data_sc456/VISPsfGrid/'

def cut_grid(VIS_grid,num_CCD,grid_size):

    """

    *** Return the array of PSF grid cutting for 1 PSF per image => return grid_size x grid_size images in the array

    num_CCD is the numero of the CCD we want to have the PSF grid (num_CCD=1 to num_CCD=36)

    VIS_grid is the fits file in "path" with different grid size
    Example : 'snap_EUC_VIS_EXP_1.fits_sa_por_M_Stars_9x9.fits'

    grid_size is the size of the grid we use in VIS_grid
    Example : 9 or 3 or 1

    """
    
    image=fits.open(path+VIS_grid)[num_CCD].data
    image=image/np.max(image)
    shape=image.shape[0] # Square images
    grid=np.empty((grid_size,grid_size),np.ndarray)
    for i in range(grid_size):
        for j in range(grid_size):
            psf=image[i*shape/grid_size:(i+1)*shape/grid_size,j*shape/grid_size:(j+1)*shape/grid_size]
            grid[i][j]=psf
            
    return grid




def cut_FPA_VIS(VIS_grid,grid_size):


      """

    *** Return the FPA array with each CCD cut in a grid 1x1, 3x3, 9x9


    VIS_grid is the fits file in "path" with different grid size
    Example : 'snap_EUC_VIS_EXP_1.fits_sa_por_M_Stars_9x9.fits'

    grid_size is the size of the grid we use in VIS_grid
    Example : 9 or 3 or 1

    """
    
    FPA=np.empty((6,6),np.ndarray)
    for j in range(6):
        for i in range(6):
            FPA[i][j]=cut_grid(VIS_grid,1+i+j*6,grid_size)

    return FPA



            
    
    
            
            
            
    
