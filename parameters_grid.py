import parameters as p
import FITS as FITS
import astropy.io.fits as fits
import PSF_monochrom_plot as PSF
import numpy as np
import matplotlib.pyplot as pl
import os


def parameters_grid_moments(grid,parameter):

    """
    *** Compute the grid of parameters for a PSF cut grid as an input

    grid is the input PSF cut grid from VIS PSF grid
    
    parameter is the parameter we want to compute
    Example: 'ellipticity' or 'theta'

    """
    grid_size=grid.shape
    parameters_grid=np.zeros(grid_size)
    for i in range(grid_size[0]):
        for j in range(grid_size[1]):
            parameters_grid[i][j]=p.compute_moments_apodized(grid[i][j],parameter)
            
    return parameters_grid

            



def parameters_grid_gaussian(grid,parameter):

    """
    *** Compute the grid of parameters for a PSF cut grid as an input

    grid is the input PSF cut grid from VIS PSF grid
    
    parameter is the parameter we want to compute
    Example: 'ellipticity' or 'fwhm_x'

    """

    grid_size=grid.shape
    parameters_grid=np.zeros(grid_size)
    for i in range(grid_size[0]):
        for j in range(grid_size[1]):
            
            if parameter=='ellipicity':
                parameters_grid[i][j]=p.ellipticity_fit(grid[i][j])

            if parameter=='FWHM_x':
                parameters_grid[i][j]=p.FWHMs_fit(grid[i][j])[0]

            if parameter=='FWHM_y':
                parameters_grid[i][j]=p.FWHMs_fit(grid[i][j])[1]

            if parameter=='theta_fit':
                parameters_grid[i][j]=p.theta_fit(grid[i][j])
                            
            
    return parameters_grid



def parameters_grid_rayons(grid,rayon):

    """
    *** Compute the grid of r68 and r95 parameters for a PSF cut grid as an input

    grid is the input PSF cut grid from VIS PSF grid
    
    rayon is the parameter we want to compute
    Example: 'r68' or 'r95'

    """

    grid_size=grid.shape
    parameters_grid=np.zeros(grid_size)
    for i in range(grid_size[0]):
        for j in range(grid_size[1]):
            image=grid[i][j]
            image=image/np.max(image)
            profile=p.radial_profile(image)
            print(len(profile))
            energie_list=p.energie_cumulated_pourcentage(profile)
            print(len(energie_list))

            if rayon=='r68':
                
                parameters_grid[i][j]=p.rayons_energie(energie_list)[0]

            if rayon=='r95':

                parameters_grid[i][j]=p.rayons_energie(energie_list)[1]
            
    return parameters_grid




def parameters_FPA_moments(parameter,FPA,grid_size):

    """
    *** Compute the grid of parameters for a FPA cut grid as an input
        with method moments

    FPA is the input FPA cut grid from VIS PSF grid
    
    parameter is the parameter we want to compute
    Example: 'ellipticity' or 'theta'
    
    grid_size is the size of grid we want for each CCD
    Example: 3 if we want a grid 3x3 per CCD

    """

    parameter_FPA=np.zeros((6*grid_size,6*grid_size))
    FPA_copy=FPA.copy()
    for i in range(6):
        for j in range(6):
            FPA_copy[i][j]=parameters_grid_moments(FPA[i][j],parameter)
            
    for row in range(6*grid_size):
        for column in range(6*grid_size):
            parameter_FPA[row][column]=FPA_copy[int(row/grid_size)][int(column/grid_size)][row%grid_size][column%grid_size]
    return parameter_FPA



def parameters_FPA_gaussian(parameter,FPA,grid_size):

    """
    *** Compute the grid of parameters for a FPA cut grid as an input
        with gaussian fit method

    FPA is the input FPA cut grid from VIS PSF grid
    
    parameter is the parameter we want to compute
    Example: 'FWHM_x'
    
    grid_size is the size of grid we want for each CCD
    Example: 3 if we want a grid 3x3 per CCD

    """

    parameter_FPA=np.zeros((6*grid_size,6*grid_size))
    FPA_copy=FPA.copy()

    for i in range(6):
        for j in range(6):
            FPA_copy[i][j]=parameters_grid_gaussian(FPA[i][j],parameter)
            
    for row in range(6*grid_size):
        for column in range(6*grid_size):
            parameter_FPA[row][column]=FPA_copy[int(row/grid_size)][int(column/grid_size)][row%grid_size][column%grid_size]
    return parameter_FPA
                   
    

def parameters_FPA_rayon(FPA,grid_size,rayon):

    """
    *** Compute the grid of rayons for a FPA cut grid as an input

    FPA is the input FPA cut grid from VIS PSF grid
    
    rayon is the r68 or r95
  
    grid_size is the size of grid we want for each CCD
    Example: 3 if we want a grid 3x3 per CCD

    """

    parameter_FPA=np.zeros((6*grid_size,6*grid_size))
    FPA_copy=FPA.copy()
    for i in range(6):
        for j in range(6):
            FPA_copy[i][j]=parameters_grid_rayons(FPA[i][j],rayon)
            
    for row in range(6*grid_size):
        for column in range(6*grid_size):
            parameter_FPA[row][column]=FPA_copy[int(row/grid_size)][int(column/grid_size)][row%grid_size][column%grid_size]
    return parameter_FPA
