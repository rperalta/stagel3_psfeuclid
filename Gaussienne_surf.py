#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 16:14:54 2019

@author: sclery
"""

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D



def gaussienne_2d(amplitude, xo, yo, sigma_x, sigma_y, theta):

    """
    *** Compute gaussian surface in 2D with (xo,yo) center of the gaussian
        sigma_x and sigma_y the sigmas along the two axes
        theta the angle of inclination of the gaussian w.r.t x_axis

    """
    
    x = np.arange(0, 50, 1)
    y = np.arange(0, 50, 1)           
    x,y=np.meshgrid(x,y)
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)   
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)    
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2) 
    
    z = amplitude * np.exp( - (a*(x-xo)**2 + 2*b*(x-xo)*(y-yo) + c*(y-yo)**2))

    return (x,y,z)




