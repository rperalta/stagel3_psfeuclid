import parameters as p
import FITS as FITS
import astropy.io.fits as fits
import PSF_monochrom_plot as PSF
import numpy as np
import matplotlib.pyplot as pl
import os



chemin="PSF_eff_sc456_SED_O/"


def liste(chemin,num_ccd):

    """
    listing the fits files in the directory from the path 'chemin'
    in the list PSFfiles

    """
    
    path=str(chemin)+"PSF_eff_sc456_SED_O"+"_CCD"+str(num_ccd)
    allfiles= os.listdir(path)
    PSFfiles= [os.path.join(path,fil)
               for fil in allfiles
                if fil.endswith('.fits')]
    return PSFfiles


# chemin="PSF_eff_sc456_SED_M/"


def parameter_card_gaussian(parameter,chemin,num_ccd):

    """
    Compute parameter card for the CCD num_ccd with the gaussian fit method

    parameter is the parameter we choose 
    Example: 'FWHM_x'

    """
    
    card_position_x=[]
    card_position_y=[]
    card_parameter=[]
    fichier=open("ccd_"+str(num_ccd)+"_"+str(parameter)+"_fit.txt","w")
    fits_list=liste(chemin,num_ccd)
    for psf in fits_list:
        positions=FITS.position(psf)
        position_x=positions[0]
        position_y=positions[1]
        image=fits.open(psf)[0].data
        pixel_max=np.max(image)
        image=image/pixel_max

        if parameter=='ellipticity':
            module_e=p.ellipticity_fit(image)
            print(module_e)
            card_parameter.append(module_e)
            card_position_x.append(position_x)
            card_position_y.append(position_y)
            fichier.write(str(module_e)+"\t")
            fichier.write(str(position_x)+"\t")
            fichier.write(str(position_y)+"\n")

        if parameter=='FWHM_x':
            FWHM=p.FWHMs_fit(image)
            FWHM_x=FWHM[0]
            print(FWHM_x)
            card_parameter.append(FWHM_x)
            card_position_x.append(position_x)
            card_position_y.append(position_y)
            fichier.write(str(FWHM_x)+"\t")
            fichier.write(str(position_x)+"\t")
            fichier.write(str(position_y)+"\n")
            
        if parameter=='FWHM_y':
            FWHM=p.FWHMs_fit(image)
            FWHM_y=FWHM[1]
            print(FWHM_y)
            card_parameter.append(FWHM_y)
            card_position_x.append(position_x)
            card_position_y.append(position_y)
            fichier.write(str(FWHM_y)+"\t")
            fichier.write(str(position_x)+"\t")
            fichier.write(str(position_y)+"\n")
            
        if parameter=='theta_fit':
            theta=p.theta_fit(image)
            print(theta)
            card_parameter.append(theta)
            card_position_x.append(position_x)
            card_position_y.append(position_y)
            fichier.write(str(theta)+"\t")
            fichier.write(str(position_x)+"\t")
            fichier.write(str(position_y)+"\n")
            
            
      
        
    fichier.close()
           
    return (card_parameter,card_position_x,card_position_y)




def parameter_card_moments(parameter,chemin,num_ccd):

    
    """
    Compute parameter card for the CCD num_ccd with the moments apodized method

    parameter is the parameter we choose 
    Example: 'ellipticity'

    """

    card_position_x=[]
    card_position_y=[]
    card_parameter=[]
    fichier=open("ccd_"+str(num_ccd)+"_"+str(parameter)+"_moments.txt","w")
    fits_list=liste(chemin,num_ccd)
    for psf in fits_list:
        positions=FITS.position(psf)
        position_x=positions[0]
        position_y=positions[1]
        image=fits.open(psf)[0].data
        parameters=p.compute_moments_apodized(image,parameter)
        print(parameters)

        card_parameter.append(parameters)
        card_position_x.append(position_x)
        card_position_y.append(position_y)
        fichier.write(str(parameters)+"\t")
        fichier.write(str(position_x)+"\t")
        fichier.write(str(position_y)+"\n")
        
        
    fichier.close()
        
    return (card_parameter,card_position_x,card_position_y)
 

       
def parameter_card_rayon(chemin,num_ccd):

    
    """
    Compute rayons parameter card for the CCD num_ccd

    Return the tuple (card_r68,card_r95,card_x,card_y)

    """

    fichier=open("ccd_"+str(num_ccd)+"_"+'rayons.txt','w')
    card_position_x=[]
    card_position_y=[]
    card_parameter_r68=[]
    card_parameter_r95=[]
    fichier_erreur=open("mauvais_rayons.txt","w")
    fits_list=liste(chemin,num_ccd)
    for psf in fits_list:
        image=fits.open(psf)[0].data
        position=FITS.position(psf)
        position_x=position[0]
        position_y=position[1]
        profile=p.radial_profile(image)
        energie_list=p.energie_cumulated_pourcentage(profile)
    
        try:
            rayons=p.rayons_energie(energie_list)
            print(rayons)
            fichier.write(str(rayons[0])+"\t")
            fichier.write(str(rayons[1])+"\t")
            fichier.write(str(position_x)+"\t")
            fichier.write(str(position_y)+"\n")
            
            card_parameter_r68.append(rayons[0])
            card_parameter_r95.append(rayons[1])
            card_position_x.append(position_x)
            card_position_y.append(position_y)
            
        except:
            print("impossible de realiser opt.bisect")
            fichier_erreur.write(psf+"\n")
            
            
    fichier.close()
    fichier_erreur.close()
            
        
    return (card_parameter_r68,card_parameter_r95,card_position_x,card_position_y)
        





