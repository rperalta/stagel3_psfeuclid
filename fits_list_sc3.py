
import astropy.io.fits as fits
import parameters as p
import FITS as FITS
import numpy as np
import matplotlib.pyplot as pl
from matplotlib import cm
import os
import scipy.optimize as opt
import scipy.interpolate as inter


# Listing the SC3 PSF eff files

path="PSF_eff_sc3/"

allfiles = os.listdir(path)
PSFfiles = [os.path.join(path,fil)
            for fil in allfiles
            if fil.endswith('.fits')]


def parameter_card_moments(parameter,fits_list):

    """
    *** Compute parameters card with method of moments for SC3 datas
    *** Return the card of r68 and r95 as a list, 
        position x in the FPA as a list
        and position y in the FPA as a list

    parameter is the name of the parameter you want to compute 
    Example : 'ellipticity' or 'theta'

    fits_list is the list of SC3 PSF eff files here it is 'PSFfiles'
 
    """
    
    card_position_x=[]
    card_position_y=[]
    card_parameter=[]
    for psf in (fits_list):
        psf=str(psf)
        image=fits.open(psf)[0].data
        position=FITS.position(psf)
        position_x=position[0]
        position_y=position[1]
        parameters=p.compute_parameters_moments(image,parameter)
        print(parameters)
        card_parameter.append(parameters)
        card_position_x.append(position_x)
        card_position_y.append(position_y)
    return (card_parameter,card_position_x,card_position_y)
    

def parameter_card_fit(parameter,fits_list):

    """
    *** Compute parameters card with method of gaussian fitting for SC3 datas
    *** Return the card as a list, position x in the FPA as a list
        and position y in the FPA as a list

    parameter is the name of the parameter you want to compute 
    Example : 'ellipticity_fit' or 'FWHM_x'

    fits_list is the list of SC3 PSF eff files here it is 'PSFfiles'
 
    """

    fit_error=open(str(parameter)+'fit_error.txt','w')
    value_error=open(str(parameter)+'value_error.txt','w')
    card_position_x=[]
    card_position_y=[]
    card_parameter=[]
    for psf in (fits_list):
        psf=str(psf)
        image=fits.open(psf)[0].data
        position=FITS.position(psf)
        position_x=position[0]
        position_y=position[1]
    
        if parameter=='ellipticity':
            module_e=p.ellipticity_fit(image)
            if module_e!=None:
                if module_e<0.5:
                    print(module_e)
                    card_parameter.append(module_e)
                    card_position_x.append(position_x)
                    card_position_y.append(position_y)
                else:
                    print(module_e)
                    print('Not a physical value')
                    value_error.write(psf+"\n")
                    
            if module_e==None:
                fit_error.write(psf+"\n")

                
        if parameter=='FWHM_x':
            
            FWHM=p.FWHMs_fit(image)
            FWHM_x=FWHM[0]
            if FWHM[0]!=None:
                if abs(FWHM[0])<8 and abs(FWHM[0])>6:
                    print(FWHM_x)
                    card_parameter.append(FWHM_x)
                    card_position_x.append(position_x)
                    card_position_y.append(position_y)
                else:
                    print(FWHM_x)
                    print('Not a physical value')
                    value_error.write(psf+"\n")
        
                    
            if FWHM[0]==None: 
                fit_error.write(psf+"\n")
                
        if parameter=='FWHM_y':
            
            FWHM=p.FWHMs_fit(image)
            FWHM_y=FWHM[1]
            if FWHM[1]!=None:
                if abs(FWHM[1])<10 and abs(FWHM[1])>5:
                    print(FWHM_y)
                    card_parameter.append(FWHM_y)
                    card_position_x.append(position_x)
                    card_position_y.append(position_y)
                else:
                    print(FWHM_y)
                    print('Not a physical value')
                    value_error.write(psf+"\n")
                         
            if FWHM[0]==None: 
                fit_error.write(psf+"\n")
                       
        if parameter=='theta_fit':
            theta=p.theta_fit(image)
            if theta!=None:
                fich_error.write(psf)
                card_parameter.append(theta)
                card_position_x.append(position_x)
                card_position_y.append(position_y)
            if theta==None or abs(theta)>180:
                fit_error.write(psf+"\n")
                
    return (card_parameter,card_position_x,card_position_y)
        
def parameter_card_rayon(fits_list):

    """
    *** Compute energie circles card with method of moments for SC3 datas
    *** Return the card as a list, position x in the FPA as a list
        and position y in the FPA as a list

    fits_list is the list of SC3 PSF eff files here it is 'PSFfiles'
 
    """
    
    card_position_x=[]
    card_position_y=[]
    card_parameter_r68=[]
    card_parameter_r95=[]
    fichier_erreur=open("mauvais_rayons.txt","w")
    for psf in (fits_list):
        psf=str(psf)
        image=fits.open(psf)[0].data
        position=FITS.position(psf)
        x=position[0]
        y=position[1]
        profile=p.radial_profile(image)
        energie_list=p.energie_cumulated_pourcentage(profile)
        try:
            rayons=p.rayons_energie(energie_list)
            card_parameter_r68.append(rayons[0])
            card_parameter_r95.append(rayons[1])
            card_position_x.append(x)
            card_position_y.append(y)
            print(rayons)
        except:
            print("impossible de realiser opt.bisect")
            fichier_erreur.write(psf+"\n")
    fichier_erreur.close()
    
        
    return (card_parameter_r68,card_parameter_r95,card_position_x,card_position_y)
        
             
        
