import numpy as np
import matplotlib.pyplot as pl



def creation_fichier(type_fichier,parametre_total):

    """
    ***Create the file 'parametre_total.txt' that contains parameter value in first column, 
       position x in the FPA in second column 
       and position y in the FPA in third column
    
    type_fichier is the name of parameters from the files of parameters per CCD
    Example : 'ellipticity_moments'

    parameter_total is the name of parameters you want to compute for the whole FPA 
    Example : 'ellipticity_moments_sed_O_FPA'

    """

    
    
    fichier=open(parametre_total+"."+"txt","a")

    
    for j in range(6):
        for k in range(6):
            files=open("ccd_"+str(j)+str(k)+"_"+type_fichier+".txt","r")
            ccd=np.loadtxt(files)
            para=ccd[:,0]
            x,y=ccd[:,1],ccd[:,2]
            for i in range(len(para)):
                fichier.write(str(para[i])+"\t"+str(x[i])+"\t"+str(y[i])+"\n")
            
    return 




def creation_rayons(type_fichier,parametre_total):

    """
    ***Create the file 'rayons_total.txt' that contains r68 in first column, 
       r95 in second colmumn
       position x in the FPA in thrid column 
       and position y in the FPA in fourth column
    
    type_fichier is the name of parameters from the files of parameters per CCD
    Example : 'rayons'

    parameter_total is the name of parameters you want to compute for the whole FPA 
    Example : 'rayons_sed_O_FPA'

    """

    
    
    fichier=open(parametre_total+"."+"txt","a")

    
    for j in range(6):
        for k in range(6):
            files=open("ccd_"+str(j)+str(k)+"_"+type_fichier+".txt","r")
            ccd=np.loadtxt(files)
            r68=ccd[:,0]
            r95=ccd[:,1]
            x,y=ccd[:,2],ccd[:,3]
            for i in range(len(r68)):
                fichier.write(str(r68[i])+"\t"+str(r95[i])+"\t"+str(x[i])+"\t"+str(y[i])+"\n")
            
    return 
