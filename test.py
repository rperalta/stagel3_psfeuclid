
import FITS as FITS
import parameters as p
import PSF_monochrom_plot as PSF
import matplotlib.pyplot as pl
import os
import numpy as np
import scipy.optimize as opt

'''
fits_name="/data/glx-herschel/data2/data_lodeen/data_sc456/psf_simulated_sc456/PSF_CCD_ccd00.fits"
image=PSF.PSF_monochrom_1_case_image(fits_name,6,1245)
'''
'''
print('(e,theta)=')
print(p.compute_parameters_moments(image))
'''

'''
print('(FWHM_x,FWHM_y)=')
print(p.FWHMs_fit(image,initial_guess))
'''
'''
print('ellipticity_fit=')
print(p.ellipticity_fit(image,initial_guess))
'''

'''
print('theta_fit=')
print(p.theta_fit(image,initial_guess))
'''
'''

initial_guess=(1,150,150,2,2,0)
image_fitted=p.gaussian_fitting_param(image,initial_guess)[2]
center_fit=p.compute_center(image_fitted,'gaussian_fit',initial_guess)
center_image=p.compute_center(image)
cursor_image=FITS.cursor(image,center_image[0],center_image[1])
cursor_fit=FITS.curosr(image_fitted,center_fit[0],center_fit[1])
pl.subplot(2,2,1)
pl.imshow(image)

pl.subplot(2,2,2)
pl.imshow(image_fitted)

'''

path="PSF_eff_sc3/"

allfiles = os.listdir(path)
PSFfiles = [os.path.join(path,fil)
            for fil in allfiles
            if fil.endswith('.fits')]


# Compute gaussian_2d profile
def gaussienne_2d(data_coordinate,amplitude, xo, yo, sigma_x, sigma_y, theta):

    (x,y)=data_coordinate
    xo=float(xo)
    yo=float(yo)                            
    
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)   
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)    
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2) 
    
    g = amplitude* np.exp( - (a*(x-xo)**2 + 2*b*(x-xo)*(y-yo) + c*(y-yo)**2))
    
    return g.ravel()

# Compute gaussian fitting from initial guess (Amplitude,x_barre,y_barre,sigma_x,sigma_y,theta)
def gaussian_fitting_param(image,initial_guess):
    size=np.shape(image)
    num_column=size[0]
    num_row=size[1]
    columns= np.arange(0,num_column, 1)
    rows= np.arange(0,num_row, 1)
    columns,rows=np.meshgrid(columns,rows)
    # Fitting gaussian
    try:
        param_opt, param_cov = opt.curve_fit(gaussienne_2d, (columns,rows), image.ravel(), p0=initial_guess)
        image_fitted = gaussienne_2d((columns,rows), *param_opt).reshape(num_column,num_row) # .reshape from 1D to 2D
    except:
        print('erreur lors du fit')
        param_opt=np.ones(6)*-1 
        param_cov=np.ones((6,6))*-1
        image_fitted=np.ones(size)*-1
    return (param_opt,param_cov,image_fitted)




