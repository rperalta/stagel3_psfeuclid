from math import *
import FITS as F
import PSF_monochrom_plot as PSF
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import scipy.interpolate as inter


def moments(image, i, j):
    """
    
    Compute image moment order (i,j) from the formula
    
    """
    
    ny, nx = image.shape

    return np.sum(np.arange(nx)**i * np.arange(ny)[:, np.newaxis]**j * image)




# NOTE : this method to find the centroide introduce an approximation of the position
# because of the moments order approximation
# => It is recommanded to use the function "positioN_pixel_max" from the procedure FITS

def compute_centroide(image):

    """
    Compute centroide coordinate (x_barre,y_barre) with moments method

    """
    
    M00 = moments(image, 0, 0)    
    M10 = moments(image, 1, 0)    
    M01 = moments(image, 0, 1)
    x_barre =int(round( M10/M00) )   
    y_barre =int(round( M01/M00))
    return (x_barre,y_barre)



def position_pixel_max(image):

    """
    Finding position (x_barre,y_barre) of the pixel with maximum intensity on image

    """

    (ymax, xmax) = np.unravel_index(np.argmax(image), image.shape)
    return (xmax,ymax)


def compute_center(image,method='max',initial_guess='none'):

    """
    Compute PSF center
    
    method is primary set on 'max' that uses function positio_pixel_max
    other methods : 'centroide' with momennts or 'gaussian_fit' which needs an initial_guess

    """
    
    if method=='max':
        peak=position_pixel_max(image)
        test=True
        for p1 in range(-1,2):
            for p2 in range(-1,2):
                if image[peak[0]+p1][peak[1]+p2]>0.95*image[peak[0]][peak[1]]:
                    test=False
        if test:
            return peak
        else:
            return compute_centroide(image)
    elif method=='centroide':
        return compute_centroide(image)
    elif method=='gaussian_fit':
        gaussian_fit=gaussian_fitting_param(image,initial_guess)
        param_opt=gaussian_fit[0]
        x_barre=param_opt[1]
        y_barre=param_opt[2]
        return(x_barre,y_barre)



def moments_centered(image, i, j, centroide):

    """
    Compute image moments centered on centroide (x_barre,y_barre), from the formula
    
    """
    
    x_barre=centroide[0]
    y_barre=centroide[1]

    ny, nx = image.shape

    return np.sum((np.arange(nx)-x_barre)**i * (np.arange(ny)-y_barre)[:, np.newaxis]**j * image)




def compute_parameters_moments(image,parameter):

    # Ellipticity is defined by e=e1+i*e2

    """
    Compute ellipticity and theta from moments 

    """
    
    M00 = moments(image, 0, 0)    
    M10 = moments(image, 1, 0)    
    M01 = moments(image, 0, 1)
    
    X =M10/M00
    Y =M01/M00
    centroide=(X,Y)
    
    # Compute e1 and e2 from the formula given in doc
    
    e1=(moments_centered(image,2,0,centroide)-moments_centered(image,0,2,centroide))/(moments_centered(image,2,0,centroide)+moments_centered(image,0,2,centroide))
    e2=2*moments_centered(image,1,1,centroide)/(moments_centered(image,2,0,centroide)+moments_centered(image,0,2,centroide))
    module_e=sqrt(e1*e1+e2*e2)
    theta = - np.arctan(e2/e1) / 2 * 180 / np.pi
    if parameter=='ellipticity':
        return module_e
    if parameter=='theta':
        return theta

def compute_moments_apodized(image,parameter):

    """
    Compute moments apodized with a gaussian profile
    Gaussian profile is simply set on the centroide of the image and with a fixed width

    """

    x,y=np.shape(image)
    x=np.arange(x)
    y=np.arange(y)[:, np.newaxis]

    
    M00 = moments(image, 0, 0)    
    M10 = moments(image, 1, 0)    
    M01 = moments(image, 0, 1)
    
    X = x - M10/M00
    Y = y - M01/M00
    
    gauss_xy = np.exp( - (X**2 + Y**2) / (2 * 45**2)) #sigma = 45 (cf doc)
    
    sum11 = (X * Y * image * gauss_xy).sum()
    sum20 = (X**2 * image * gauss_xy).sum()
    sum02 = (Y**2 * image * gauss_xy).sum()
    sum00 = (image * gauss_xy).sum()
    
    Qxy = sum11 / sum00
    Qxx = sum20 / sum00
    Qyy = sum02 / sum00

    
    R2 = Qxx + Qyy
    e1 = (Qxx - Qyy) / R2
    e2 = 2*Qxy / R2
    e  = np.sqrt(e1*e1 + e2*e2)
    theta = - np.arctan(e2/e1) / 2 * 180 / np.pi

    if parameter=='theta':
        return theta

    if parameter=='ellipticity':
        return e




def radial_profile(image):

    """
    
    Compute azimutal (radial) mean profile of the PSF

    """
    centre=position_pixel_max(image)
    x_barre=centre[0]
    y_barre =centre[1]
    y, x = np.indices((image.shape))
    r = np.sqrt((x - x_barre)**2 + (y - y_barre)**2)
    r = r.astype(np.int)
    tbin = np.bincount(r.ravel(), image.ravel())
    nr = np.bincount(r.ravel())
    profile = tbin / nr
    return profile



def energie_cumulated_pourcentage(profile):


    """
    Compute pourcentage of the energie cumulated at each radius (pixel from the center)

    """
    
    L = [profile[0]]
    
    for i in range(1,len(profile)): 
        L.append(L[-1] + profile[i])
    L=np.array(L)
        
    return L/profile.sum()



def rayons_energie(energie_cumulated):

    """

    Compute the value of r68 and r95 radius (cf doc)

    """

    x = np.arange(len(energie_cumulated))
    
    inte68 = energie_cumulated - 0.683
    f68 = inter.interp1d(x, inte68, kind='cubic')
    r68 =opt.bisect(f68, 0, 25)
    
    inte95 = energie_cumulated - 0.955
    f95 =inter.interp1d(x, inte95, kind='cubic')
    r95 = opt.bisect(f95, 0, 25)
    
    return (r68, r95)


def gaussienne_2d(data_coordinate,amplitude, xo, yo, sigma_x, sigma_y, theta):

    """
    Compute gaussian_2d profile

    """

    (x,y)=data_coordinate
    xo=float(xo)
    yo=float(yo)                            
    
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)   
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)    
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2) 
    
    g = amplitude* np.exp( - (a*(x-xo)**2 + 2*b*(x-xo)*(y-yo) + c*(y-yo)**2))
    
    return g.ravel()


def gaussian_fitting_param(image):


    """
    
    Compute gaussian fitting from initial guess (Amplitude,x_barre,y_barre,sigma_x,sigma_y,theta)

    """
    
    size=np.shape(image)
    num_column=size[0]
    num_row=size[1]
    columns= np.arange(0,num_column, 1)
    rows= np.arange(0,num_row, 1)
    columns,rows=np.meshgrid(columns,rows)
    (ymax, xmax) = position_pixel_max(image)
    initial_guess = (1, xmax, ymax, 2.5, 2.5, 0)

    # Fitting gaussian
    
    try:
        param_opt, param_cov = opt.curve_fit(gaussienne_2d, (columns,rows), image.ravel(), p0=initial_guess)
        image_fitted = gaussienne_2d((columns,rows), *param_opt).reshape(num_column,num_row) # .reshape from 1D to 2D
        return (param_opt,param_cov,image_fitted)

    except:
        param_opt_error=np.ones(6)*180
        param_cov_error=np.ones((6,6))*180
        image_fitted_error=np.ones(size)*0
        print('erreur lors du fit')
        return (param_opt_error,param_cov_error,image_fitted_error)
    

def FWHMs_fit(image):

    """
    
    Compute FWHM from the gaussian fit
    This method return None for non physical values

    """
    gaussian_fit=gaussian_fitting_param(image)
    param_opt=gaussian_fit[0]
    if param_opt[3]!=180 and param_opt[4]!=180:
        sigma_x_fit=param_opt[3]
        sigma_y_fit=param_opt[4]
        FWHM_x=2*sqrt(2*log(2))*sigma_x_fit
        FWHM_y=2*sqrt(2*log(2))*sigma_y_fit
    else:
        FWHM_x=None
        FWHM_y=None
    return (FWHM_x, FWHM_y)


def ellipticity_fit(image):

    """
    Compute module of ellipticity from the gaussian_fit
    This method return None for non physical values

    """
    
    FWHM=FWHMs_fit(image)
    if FWHM!=(None,None):
        fwhm_x=FWHM[0]
        fwhm_y=FWHM[1]
        module_e=fabs((fwhm_x**2-fwhm_y**2)/(fwhm_x**2+fwhm_y**2))
        return module_e
    else:
        return None



def theta_fit(image):

    """
    Compute inclination angle with the gaussian_fit
    This method return None for non physical values

    """
    
    theta=gaussian_fitting_param(image)[0][5]*180/np.pi
    if theta!=180:
        return theta
    else:
        return None

    

