
import parameters as p
import numpy as np
import matplotlib.pyplot as pl
import PSF_monochrom_plot as PSF
import astropy.io.fits as fits

# PLOT TEST

fits_name="PSF_eff_sc3/psf_effective_X-0.092077_Y1.009154.fits"
data=fits.open(fits_name)
image=data[0].data
initial_guess=(1,256,256,5,5,0)
image_fitted=p.gaussian_fitting_param(image,initial_guess)[2]

'''
# Two methods to find the centroide : position_pixel_max is better
rad=p.radial_profile(image)
energie=p.energie_cumulated_pourcentage(rad)
(r68,r95)=p.rayons_energie(energie)
print(r68,r95)

pl.subplot(1,2,1)
pl.plot(rad)
pl.xlim(-0.1,50)
pl.ylim(-0.1,1)

pl.subplot(1,2,2)
pl.plot(energie)
pl.xlim(-0.1,50)
pl.ylim(-0.1,1)

pl.show()
<<<<<<< HEAD
=======



>>>>>>> 054e4270d55baf18d9f7208636855aa8ac13378b
'''
'''
pixel_max=p.compute_center(image,'gaussian_fit',initial_guess)
xmax=int(round(pixel_max[0]))
ymax=int(round(pixel_max[1]))


plt.subplot(2, 2, 1)
plt.title("Tranche a x fixe")

plt.plot(image[:,xmax])
plt.plot(image_fitted[:,xmax])
plt.axis([200 , 400, -0.1, 1.1])
plt.legend()

plt.subplot(2, 2, 3)
plt.title("Tranche a y fixe")

plt.plot(image[ymax,:])
plt.plot(image_fitted[ymax,:])
plt.axis([200 , 400, -0.1, 1.1])


# Image
plt.subplot(1, 2, 2)

im = plt.imshow(image)
plt.plot([xmax,xmax], [0,500], label='Tranche a x fixe')
plt.plot([0,500], [ymax,ymax], label='Tranche a y fixe')
plt.axis([200 , 400, 200, 400])
plt.colorbar(im)
plt.legend()


plt.show()

'''




        
